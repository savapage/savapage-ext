<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->

# savapage-ext

SavaPage Extension Interface for application plug-ins.
 
### License

This module is part of the SavaPage project <https://www.savapage.org>,
copyright (c) 2011-2020 Datraverse B.V. and licensed under the
[GNU Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl.html)
version 3, or (at your option) any later version.

[<img src="./img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)

### Join Efforts, Join our Community

SavaPage Software is produced by Community Partners and consumed by Community Members. If you want to modify and/or distribute our source code, please join us as Development Partner. By joining the [SavaPage Community](https://wiki.savapage.org) you can help build a truly Libre Print Management Solution. Please contact [info@savapage.org](mailto:info@savapage.org).

### Issue Management

[https://issues.savapage.org](https://issues.savapage.org)

### Usage
  
Edit `/opt/savapage/server/lib/log4j.properties` and add:

    #------------------------------------------------------------------------------
    # Payment Gateway Audit Trail
    #------------------------------------------------------------------------------
    log4j.appender.paymentgateway=org.apache.log4j.RollingFileAppender
    log4j.appender.paymentgateway.MaxFileSize=10MB
    log4j.appender.paymentgateway.MaxBackupIndex=10
    log4j.appender.paymentgateway.File=${server.home}/logs/paymentgateway.log
    log4j.appender.paymentgateway.layout=org.apache.log4j.PatternLayout
    log4j.appender.paymentgateway.layout.ConversionPattern=%m\n
    log4j.appender.paymentgateway.encoding=UTF8
    
    log4j.logger.org.savapage.server.ext.PaymentGatewayLogger=INFO, paymentgateway
    log4j.additivity.org.savapage.server.ext.PaymentGatewayLogger=false

